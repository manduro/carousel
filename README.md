# Motivation of choices

My main motivation in the design was making the assignment a fun learning experience, by using some pretty new browser features:

- Fullscreen API
- Intersection Observer API
- CSS Scroll Snap Points (and `scroll-behavior`)
- ES6 syntax

Because of this it's very experimental, but the basics of viewing and navigating slides should work in any browser. In the browsers that do support these fancy features the carousel is progressively enhanced.
