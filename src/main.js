const selectors = {
  carousels: '.carousel',
  scrollContainer: '.scroll',
  slides: 'li',
  previousBtn: '.control--previous',
  nextBtn: '.control--next',
  fullscreenBtn: '.fullscreen-toggle',
  bulletContainer: '.bullets',
  activeBulletClass: 'active'
};

document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll(selectors.carousels).forEach(carousel => {
    new Carousel(carousel);
  });
});

class Carousel {
  constructor(el) {
    this.el = el;
    this.scrollContainer = el.querySelector(selectors.scrollContainer);
    this.bulletContainer = this.el.querySelector(selectors.bulletContainer);
    this.slides = el.querySelectorAll(selectors.slides);

    this.initBullets();
    this.initButtons();
    this.initIntersectionObserver();
    this.setIndex(0);
  }

  initBullets() {
    if (this.bulletContainer) {
      for (let i = 0; i < this.length; i++) {
        const bullet = document.createElement('button');
        bullet.addEventListener('click', () => this.slideTo(i));
        this.bulletContainer.appendChild(bullet);
      }
    }
  }

  initButtons() {
    this.el.querySelectorAll(selectors.previousBtn).forEach(btn => {
      btn.addEventListener('click', () => this.slidePrevious());
    });

    this.el.querySelectorAll(selectors.nextBtn).forEach(btn => {
      btn.addEventListener('click', () => this.slideNext());
    });

    this.el.querySelectorAll(selectors.fullscreenBtn).forEach(btn => {
      if (document.webkitFullscreenEnabled) {
        btn.addEventListener('click', () => this.toggleFullscreen());
      } else {
        btn.classList.add('hidden');
      }
    });
  }

  initIntersectionObserver() {
    if ('IntersectionObserver' in window) {
      const options = {
        root: this.scrollContainer,
        threshold: 0.8
      };

      const callback = entries => {
        const currentSlide = entries.filter(entry => entry.isIntersecting)[0];
        const newIndex = getChildIndex(currentSlide.target);
        this.setIndex(newIndex);
      };

      const observer = new IntersectionObserver(callback, options);
      this.slides.forEach(el => observer.observe(el));
    }
  }

  slideTo(newIndex) {
    const slideWidth = this.scrollContainer.scrollWidth / this.length;
    this.scrollContainer.scrollLeft = newIndex * slideWidth;

    if (!('IntersectionObserver' in window)) {
      this.setIndex(newIndex);
    }
  }

  slideNext() {
    const nextIndex = (this.index + 1) % this.length;
    this.slideTo(nextIndex);
  }

  slidePrevious() {
    const previousIndex = this.index === 0 ? this.length - 1 : this.index - 1;
    this.slideTo(previousIndex);
  }

  toggleFullscreen() {
    if (document.webkitIsFullScreen) {
      document.webkitExitFullscreen();
    } else {
      this.el.webkitRequestFullscreen();
    }
  }

  get length() {
    return this.slides.length;
  }

  setIndex(newIndex) {
    this.index = newIndex;

    if (this.bulletContainer) {
      const active = this.bulletContainer.getElementsByClassName(
        selectors.activeBulletClass
      );
      if (active.length > 0) {
        active[0].classList.remove(selectors.activeBulletClass);
      }
      this.bulletContainer.children[newIndex].classList.add(
        selectors.activeBulletClass
      );
    }
  }
}

function getChildIndex(el) {
  return Array.prototype.indexOf.call(el.parentElement.children, el);
}
